// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class UnrealShooterTarget : TargetRules
{
	public UnrealShooterTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("UnrealShooter");
		// bOverrideBuildEnvironment = true;
		// AdditionalCompilerArguments = "-Wno-unused-but-set-variable";

	}
}
