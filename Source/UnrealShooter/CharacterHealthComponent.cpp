// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterHealthComponent.h"

void UCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f  && CurrentDamage < 0.0f)
	{
		ChangeShieldValue(CurrentDamage);
		if (Shield < 0.0f)
		{
			// FX
			UE_LOG(LogTemp, Warning, TEXT("UCharacterHealthComponent::ChangeHealthValue - Shield < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}

}

float UCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	
	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CallDownShield,
											   this,
											   &UCharacterHealthComponent::CallDownShieldEnd,
											   CallDownShieldRecoveryTime,
											   false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

	OnShieldChange.Broadcast(Shield, ChangeValue);
	
}

void UCharacterHealthComponent::CallDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer,
											   this,
											   &UCharacterHealthComponent::RecoveryShield,
											   ShieldRecoveryRate,
											   true);
	}
}

void UCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp += ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}
