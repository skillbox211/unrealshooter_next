// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HealthComponent.h"
#include "CharacterHealthComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);
/**
 * 
 */
UCLASS()
class UNREALSHOOTER_API UCharacterHealthComponent : public UHealthComponent
{
public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category="Shield")
	FOnShieldChange OnShieldChange;
	
	FTimerHandle TimerHandle_CallDownShield;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;
	
private:
	GENERATED_BODY()
	
protected:
	float Shield = 100.0f;
public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Shield")
	float CallDownShieldRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Shield")
	float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Shield")
	float ShieldRecoveryRate = 0.1f;
	
	
	void ChangeHealthValue(float ChangeValue) override;

	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);
	
	void CallDownShieldEnd();

	void RecoveryShield();
};
