// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealShooterGameMode.h"
#include "UnrealShooterPlayerController.h"
#include "../Character//UnrealShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUnrealShooterGameMode::AUnrealShooterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AUnrealShooterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

