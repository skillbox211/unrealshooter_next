// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UnrealShooter/FuncLibrary/Types.h"
#include "../CharacterHealthComponent.h"


#include "UnrealShooterCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnBoostStaminaChange, float, BoostStamina, float, ChangeValue);

class AWeaponDefault;
UCLASS(Blueprintable)
class AUnrealShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AUnrealShooterCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCharacterHealthComponent* HealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:
	// Cursor !!!
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cursor")
	UMaterialInterface* CursorMaterial = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();
	
	UDecalComponent* CurrentCursor = nullptr;

	// Weapons !!!

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AWeaponDefault* CurrentWeapon = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName;

	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();

	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
	void WeaponTick(APlayerController* myController);

	UFUNCTION()
	void WeaponReloadStart();

	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);

	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP();
	
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION(BlueprintCallable)
	void ChangeWeaponState();

	UFUNCTION()
	void InitWeaponEvent();

	UFUNCTION(BlueprintNativeEvent)
	void InitWeaponEvent_BP(UAnimMontage* AnimCharHit,
					        UAnimMontage* AnimCharAimHit,
					        UAnimMontage* AnimCharReload,
					        UAnimMontage* AnimCharReloadAim,
					        UAnimMontage* AnimWeaponReload,
					        UAnimMontage* AnimWeaponReloadAim,
					        UAnimMontage* AnimWeaponHit);

	UFUNCTION()
	void WeaponFireStart();

	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP();

	UFUNCTION()
	void ExpodedGranade();

	UFUNCTION(BlueprintNativeEvent)
	void ExpodedGranade_BP();
	
	
	// Movement !!!

	UFUNCTION()
	void MovementTick(APlayerController* MyController, float DeltaTime);
	
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	// float MaxSprintDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool IsMovementToCursor{false};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	int32 MaxAngleBetweenMovementAndCursor;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	FCharacterSpeed MovementInfo;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool SprintRunEnabled = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool WalkEnabled = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool AimEnabled = false;

	void BoostCharacterOrNot();
	void RotateCharacterToCursorLocation();

	// FVector LocationToBeginSprint;
	// FVector LocationToEndSprint;
	
	// Inputs !!!
	UFUNCTION()
	void InputAxisY(float Value);

	UFUNCTION()
	void InputAxisX(float Value);

	UFUNCTION()
	void InputAttackPressed();

	UFUNCTION()
	void InputAttackReleased();

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category="Movement")
	FOnBoostStaminaChange OnBoostStaminaChange;

	FTimerHandle TimerHandle_BoostRecovery;
	FTimerHandle TimerHandle_BoostDecrease;
	FTimerHandle TimerHandle_BoostIncrease;

	float BoostStamina = 100.0f;
	float BoostDecreaseRate = 2.0f;
	float BoostIncreaseRate = 2.0f;
	float BoostTimeRecoverToIncrease = 5.0f;
	float BoostTimeToIncrease = 0.1f;

	void DecreaseBoostStamina();
	void IncreaseBoostStamina();
	void ChangeBoostStamina(float ChangeValue);
	void RecoverToIncrease();

	//Inventory Func

	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();

	UPROPERTY(BlueprintReadOnly,EditDefaultsOnly)
	int32 CurrentIndexWeapon = 0;


	// Health Func
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Health")
	bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Health")
	TArray<UAnimMontage*> DeadsAnim;
	
	virtual float TakeDamage(float Damage,
							 FDamageEvent const& DamageEvent,
							 AController* EventInstigator,
							 AActor* DamageCauser) override;

	UFUNCTION()
	void CharDead();

	FTimerHandle TimerHandle_RagDollTimer;
	void EnableRagdoll();
	
};

