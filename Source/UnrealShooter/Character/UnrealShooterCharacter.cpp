// Copyright Epic Games, Inc. All Rights Reserved.


#include "UnrealShooterCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "../Weapon/WeaponDefault.h"
#include "../Game/UnrealShooterGameInstance.h"
#include "UnrealShooter/InventoryComponent.h"
#include "UnrealShooter/Game/UnrealShooterPlayerController.h"


AUnrealShooterCharacter::AUnrealShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm


	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("Inventory Component"));
	HealthComponent = CreateDefaultSubobject<UCharacterHealthComponent>(TEXT("HealthComponent"));

	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &AUnrealShooterCharacter::CharDead);
	}
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AUnrealShooterCharacter::InitWeapon);
	}
	

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	MaxAngleBetweenMovementAndCursor = 30;
	//MaxSprintDistance = 1500;
}


// ====================================================== VikBo ================================================ //
//																								       		     //
// ============================================= Work With Override Methods Bigin ============================== //


void AUnrealShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}


void AUnrealShooterCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	// if (IsValid(CurrentWeapon))
	// {
	// 	if(CurrentWeapon->WasExplosion)
	// 	{
	// 		CurrentWeapon->WasExplosion = false;
	// 		ExpodedGranade();
	// 	}
	// }
	
	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	MovementTick(MyController, DeltaSeconds);
	WeaponTick(MyController);
}


void AUnrealShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis(TEXT("MoveForward"), this, &AUnrealShooterCharacter::InputAxisX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &AUnrealShooterCharacter::InputAxisY);

	InputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &AUnrealShooterCharacter::InputAttackPressed);
	InputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &AUnrealShooterCharacter::InputAttackReleased);
	InputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Pressed, this, &AUnrealShooterCharacter::TryReloadWeapon);

	InputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &AUnrealShooterCharacter::TrySwitchNextWeapon);
	InputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &AUnrealShooterCharacter::TrySwitchPreviosWeapon);
	
}


// ============================================= Work With Override Methods End =================================== //
//																												    //
// ============================================= Work With Input Methods Begin ==================================== //

/* A, D Key  */
void AUnrealShooterCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

/* W, S Key  */
void AUnrealShooterCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

/* Left Mouse Key  */
void AUnrealShooterCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
	WeaponFireStart();
}

/* Left Mouse Key  */
void AUnrealShooterCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}


// ============================================= Work With Input Methods End ================================== //
//																												//
// ============================================= Work With Movement Begin ===================================== //


/* movement deals to tick event */
void AUnrealShooterCharacter::MovementTick(APlayerController* MyController, float DeltaTime)
{
	if (bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	
		if (MyController)
		{
			RotateCharacterToCursorLocation();
			BoostCharacterOrNot();
		}
	}
}


/*	deciding on character acceleration	*/
void AUnrealShooterCharacter::BoostCharacterOrNot()
{
	// get movement direction of character
	FVector MovementVector = FVector(1.0f, 0.0f, 0.0f)*AxisX + FVector(0.0f, 1.0f, 0.0f)*AxisY;
	float YawMovement = UKismetMathLibrary::FindLookAtRotation(FVector::ZeroVector, MovementVector).Yaw;

	// check movement direction of character and direction of cursor
	if (abs(YawMovement - GetActorRotation().Yaw) < MaxAngleBetweenMovementAndCursor)
	{
		// аctivate character boost
		if (MovementState != EMovementState::SprintRun_State)
		{
			IsMovementToCursor = true;
			GetWorldTimerManager().SetTimer(TimerHandle_BoostDecrease,
											this,
											&AUnrealShooterCharacter::DecreaseBoostStamina,
											BoostTimeToIncrease,
											true);
		}
		// decision to slow down character
		else if(IsMovementToCursor)
		{
			// FVector SprintDistance = GetActorLocation() - LocationToBeginSprint;
			// float LengthSprintDistance = abs(sqrt(pow(SprintDistance.X, 2) + pow(SprintDistance.Y, 2)));
			if (BoostStamina == 0)
			{
				IsMovementToCursor = false;
				CharacterUpdate();
			}
		}
				
	}
	// if direction cursor and movement direction are different, then boost is cancel
	else if(IsMovementToCursor)
	{
		IsMovementToCursor = false;
		CharacterUpdate();
		GetWorldTimerManager().ClearTimer(TimerHandle_BoostDecrease);
	}
}

void AUnrealShooterCharacter::DecreaseBoostStamina()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_BoostRecovery);
	GetWorldTimerManager().ClearTimer(TimerHandle_BoostIncrease);
	ChangeBoostStamina(-BoostDecreaseRate);
}

void AUnrealShooterCharacter::IncreaseBoostStamina()
{
	ChangeBoostStamina(BoostIncreaseRate);
}

void AUnrealShooterCharacter::ChangeBoostStamina(float ChangeValue)
{
	float tmp = BoostStamina + ChangeValue;
	if (tmp < 0.0f)
	{
		BoostStamina = 0;
		GetWorldTimerManager().ClearTimer(TimerHandle_BoostDecrease);
		
	} else if (tmp > 100.0f)
	{
		BoostStamina = 100.0f;
		GetWorldTimerManager().ClearTimer(TimerHandle_BoostIncrease);
	}
	else
	{
		BoostStamina = tmp;
		OnBoostStaminaChange.Broadcast(BoostStamina, ChangeValue);
	}

	GetWorldTimerManager().SetTimer(TimerHandle_BoostRecovery,
									this,
									&AUnrealShooterCharacter::RecoverToIncrease,
									BoostTimeRecoverToIncrease,
									false);
	
}

void AUnrealShooterCharacter::RecoverToIncrease()
{
	GetWorldTimerManager().SetTimer(TimerHandle_BoostIncrease,
									this,
									&AUnrealShooterCharacter::IncreaseBoostStamina,
									BoostTimeToIncrease,
									true);
	GetWorldTimerManager().ClearTimer(TimerHandle_BoostRecovery);
}


/*	if cursor change location character will be change rotation to cursor	*/
void AUnrealShooterCharacter::RotateCharacterToCursorLocation()
{
	// get vector to cursor
	FVector2D pos;
	UGameViewportClient* MyViewport = GetWorld()->GetGameViewport();
	MyViewport->GetMousePosition(pos);
	FVector VectorToCursor = FVector(pos.X, pos.Y, 0);
		
	// get center of viewport display
	const FVector2D ViewportSize = FVector2D(MyViewport->Viewport->GetSizeXY());
	const FVector2D  ViewportCenter =  FVector2D(ViewportSize.X/2, ViewportSize.Y/2);
	const FVector VectorToCenter = UKismetMathLibrary::Conv_Vector2DToVector(ViewportCenter);
		
	// rotate character to cursor position
	float YawCursor = UKismetMathLibrary::FindLookAtRotation(VectorToCenter, VectorToCursor).Yaw;
	SetActorRotation(FQuat(FRotator(0.0f, YawCursor + 90, 0.0f)));
}


/* set max speed for character from his movement state */
void AUnrealShooterCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = IsMovementToCursor ? MovementInfo.SprintRunSpeedRun : MovementInfo.RunSpeedNormal;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}


/* change movement state of character */
void AUnrealShooterCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::SprintRun_State;
		}
		else if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}

	CharacterUpdate();
	ChangeWeaponState();
}

// ============================================= Work With Movement End ======================================= //
//																												//
// ============================================= Work With Weapon Begin ======================================= //


/*	work with weapon (height of bullet trace and state of bullet`s dispersion) every tick	*/
void AUnrealShooterCharacter::WeaponTick(APlayerController* MyController)
{
	FHitResult ResultHit;
	
	MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

	if (CurrentWeapon)
	{
		FVector Displacement = FVector(0);
		switch (MovementState)
		{
		case EMovementState::Aim_State:
			Displacement = FVector(0.0f, 0.0f, 160.0f);
			CurrentWeapon->ShouldReduceDispersion = true;
			break;
		case EMovementState::AimWalk_State:
			Displacement = FVector(0.0f, 0.0f, 160.0f);
			CurrentWeapon->ShouldReduceDispersion = true;
			break;
		case EMovementState::Walk_State:
			Displacement = FVector(0.0f, 0.0f, 120.0f);
			CurrentWeapon->ShouldReduceDispersion = false;
			break;
		case EMovementState::Run_State:
			Displacement = FVector(0.0f, 0.0f, 120.0f);
			CurrentWeapon->ShouldReduceDispersion = false;
			break;
		case EMovementState::SprintRun_State:
			break;
		default:
			break;
		}

		CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;

	}
}


/* create and tune weapon object from game instance object */
void AUnrealShooterCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	
	UUnrealShooterGameInstance* myGI = Cast<UUnrealShooterGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if(myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();
				
				/*AWeaponDefault* myWeapon*/CurrentWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass,
																						  &SpawnLocation,
																						  &SpawnRotation,
																						  SpawnParams));
				if (CurrentWeapon)
				{
					
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					CurrentWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					//CurrentWeapon = myWeapon;
					
					CurrentWeapon->WeaponSetting = myWeaponInfo;
					// CurrentWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;
					// Remove !!! Debug
					CurrentWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					CurrentWeapon->UpdateStateWeapon(&MovementState);

					CurrentWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					CurrentIndexWeapon = NewCurrentIndexWeapon;

					CurrentWeapon->OnWeaponReloadStart.AddDynamic(this,&AUnrealShooterCharacter::WeaponReloadStart);
					CurrentWeapon->OnWeaponReloadEnd.AddDynamic(this,&AUnrealShooterCharacter::WeaponReloadEnd);

					CurrentWeapon->OnWeaponFireStart.AddDynamic(this, &AUnrealShooterCharacter::WeaponFireStart);

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <=0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();
					
					if(InventoryComponent)
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(CurrentWeapon->WeaponSetting.WeaponType);
					
					InitWeaponEvent();

					
				}
		
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AUnrealShooterCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}


/* start to reload weapon */
void AUnrealShooterCharacter::TryReloadWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		if(CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
		{
			CurrentWeapon->InitReload();
		}
	}
}


/* activate/deactivate weapon to firing */
void AUnrealShooterCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AUnrealShooterCharacter::AttackCharEvent - CurrentWeapon - NULL"))
	}
}


/* get current weapon */
AWeaponDefault* AUnrealShooterCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}


/* change weapon state */
void AUnrealShooterCharacter::ChangeWeaponState()
{
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(&MovementState);
	}
}

void AUnrealShooterCharacter::InitWeaponEvent()
{
	InitWeaponEvent_BP(CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimCharFire,
				       CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimCharFireAim,
				       CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimCharFireReload,
				       CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimCharFireReloadAim,
				       CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimWeaponReload,
				       CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimWeaponReloadAim,
				       CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
}

void AUnrealShooterCharacter::WeaponFireStart()
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	WeaponFireStart_BP();
}

void AUnrealShooterCharacter::WeaponFireStart_BP_Implementation()
{
	// in BP
}

void AUnrealShooterCharacter::InitWeaponEvent_BP_Implementation(UAnimMontage* AnimCharHit,
                                                                UAnimMontage* AnimCharAimHit,
                                                                UAnimMontage* AnimCharReload,
                                                                UAnimMontage* AnimCharReloadAim,
                                                                UAnimMontage* AnimWeaponReload,
                                                                UAnimMontage* AnimWeaponReloadAim,
                                                                UAnimMontage* AnimWeaponHit)
{
	// in BP
}


void AUnrealShooterCharacter::WeaponReloadStart()
{
	// if (AimEnabled)
	// 	WeaponReloadStart_BP(CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimCharFireReloadAim);
	// else
	// 	WeaponReloadStart_BP(CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimCharFireReload);
	WeaponReloadStart_BP();
}

void AUnrealShooterCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void AUnrealShooterCharacter::WeaponReloadStart_BP_Implementation()
{
	// in BP
}

void AUnrealShooterCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}


void AUnrealShooterCharacter::ExpodedGranade()
{
	ExpodedGranade_BP();
}

void AUnrealShooterCharacter::ExpodedGranade_BP_Implementation()
{
	// in BP
}

// ============================================= Work With Weapon End ========================================= //
//																												//
// ============================================= Work With Common Method Begin ================================ //




UDecalComponent* AUnrealShooterCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}


// ============================================= Work With Common Method End ================================== //
//																												//
// ============================================== Work with Inventory Begin =================================== //

void AUnrealShooterCharacter::TrySwitchNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if(CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}
			
		if (InventoryComponent)
		{			
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo,true))
			{
				
			}
		}
	}	
}

void AUnrealShooterCharacter::TrySwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1,OldIndex, OldInfo, false))
			{ }
		}
	}
}



// =============================================   Work with Inventory End   ================================== //
//																												//
// ===========================================  Work With Health Method Begin  ================================ //

float AUnrealShooterCharacter::TakeDamage(float Damage,
										  FDamageEvent const& DamageEvent,
										  AController* EventInstigator,
										  AActor* DamageCauser)
{
	if (bIsAlive)
	{
		HealthComponent->ChangeHealthValue(-Damage);
	}
	
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

void AUnrealShooterCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int8 rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}

	bIsAlive = false;
	UnPossessed();
 
	//Timer
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer,
									this,
									&AUnrealShooterCharacter::EnableRagdoll,
								    TimeAnim,
								    false);

	GetCursorToWorld()->SetVisibility(false);
	
}

void AUnrealShooterCharacter::EnableRagdoll()
{
	

	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

// ============================================= Work With Health Method End ================================== //
//																												//
// ============================================================================================================ //
