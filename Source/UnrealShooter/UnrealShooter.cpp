// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealShooter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UnrealShooter, "UnrealShooter" );

DEFINE_LOG_CATEGORY(LogUnrealShooter)
 