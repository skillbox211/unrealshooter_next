// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"

#include "DrawDebugHelpers.h"
#include "WeaponDefault.h"
#include "Kismet/GameplayStatics.h"
#include "UnrealShooter/Character/UnrealShooterCharacter.h"
#include "UnrealShooter/Game/UnrealShooterPlayerController.h"


void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

	AController* InstigatorController = GetInstigatorController();
	AUnrealShooterPlayerController* MyController = Cast<AUnrealShooterPlayerController>(InstigatorController);
	

	if (IsValid(MyController))
	{
		ACharacter* InstigatorCharacter = MyController->GetCharacter();
		AUnrealShooterCharacter* MyCharacter = Cast<AUnrealShooterCharacter>(InstigatorCharacter);
		if (IsValid(MyCharacter))
		{
			this->OnExplose.AddDynamic(MyCharacter,&AUnrealShooterCharacter::ExpodedGranade);
			ShowDebug = MyCharacter->GetCurrentWeapon()->ShowDebug;
		}
	}
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();
			
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;

	
}

void AProjectileDefault_Grenade::Explose()
{
	if (ShowDebug)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileDistanceMaxDamage, 8, FColor::Red, false, 4.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileDistanceMinDamage, 8, FColor::Blue, false, 4.0f);
	}
		
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}
	
	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage*0.2f,
		GetActorLocation(),
		ProjectileSetting.ProjectileDistanceMaxDamage,
		ProjectileSetting.ProjectileDistanceMinDamage,
		5,
		NULL, IgnoredActor,nullptr,nullptr);

	this->Destroy();

	OnExplose.Broadcast();
}
