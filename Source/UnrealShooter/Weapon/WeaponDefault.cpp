// Fill out your copyright notice in the Description page of Project Settings.
#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "UnrealShooter/InventoryComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
	
}

// ====================================================== VikBo ================================================ //
//																								       		     //
// ============================================= Work With Override Methods Bigin ============================== //

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	
	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	ShellDropTick(DeltaTime);
}

// ============================================= Work With Override Methods End =================================== //
//																												    //
// ============================================= Work With Fire Methods Begin ===================================== //


void AWeaponDefault::FireTick(float DeltaTime)
{

	if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
	{
		if (FireTimer < 0.f)
		{
			Fire();
		}
		else
		{
			FireTimer -= DeltaTime;
		}
	}

	
	// if (GetWeaponRound() > 0)
	// {
	// 	if (WeaponFiring)
	// 		if (FireTimer < 0.f)
	// 		{
	// 			if (!WeaponReloading)
	// 				Fire();
	// 		}
	// 		else
	// 			FireTimer -= DeltaTime;
	// }
	// else
	// {
	// 	if (!WeaponReloading && CheckCanWeaponReload() )
	// 	{
	// 		InitReload();
	// 	}
	// }
}


void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
	FireTimer = 0.01f;//!!!!!
}


bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}


FProjectileInfo AWeaponDefault::GetProjectile()
{	
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	if (WeaponSetting.ShellBullets.DropMesh)
	{
		if (WeaponSetting.ShellBullets.DropMeshTime < 0.0f)
		{
			InitDropMesh(WeaponSetting.ShellBullets);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}
	}
	
	FireTimer = WeaponSetting.RateOfFire;
	ChangeDispersionByShot();
	
	

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
			{
				AdditionalWeaponInfo.Round -= 1;
				
				EndLocation = GetFireEndLocation(); 

				FVector Dir = (EndLocation - SpawnLocation);
				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				if (ProjectileInfo.Projectile)
				{
					//Projectile Init ballistic fire

					FActorSpawnParameters SpawnParams;
					SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
					SpawnParams.Owner = GetOwner();
					SpawnParams.Instigator = GetInstigator();

					AProjectileDefault* MyProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
					if (MyProjectile)
					{													
						MyProjectile->InitProjectile(WeaponSetting.ProjectileSetting);

						
						// MyProjectile->OnExplose.AddDynamic(this,
						// 									&AWeaponDefault::GrenadeExploded);
					}
				}
				else
				{
					FHitResult HitTrace;
					FCollisionQueryParams Params;
					FCollisionResponseParams ResponseParam;
					Params.bReturnPhysicalMaterial = true;
					
					GetWorld()->LineTraceSingleByChannel(HitTrace,
														 ShootLocation->GetComponentLocation(),
														 EndLocation,
														 ECC_PhysicsBody,
														 Params,
														 ResponseParam);

					UPrimitiveComponent* HittedComp = HitTrace.GetComponent();
					UPhysicalMaterial* HittedSurface = HitTrace.PhysMaterial.Get();
					if(HittedComp && HittedSurface)
					{

						if (WeaponSetting.ProjectileSetting.HitDecals.Contains(HittedSurface->SurfaceType))
						{
							UMaterialInterface* HittedMaterial = WeaponSetting.ProjectileSetting.HitDecals[HittedSurface->SurfaceType];
							if (HittedMaterial)
								UGameplayStatics::SpawnDecalAttached(HittedMaterial,
																	 FVector(20.0f),
																	 HitTrace.GetComponent(),
																	 NAME_None,
																	 HitTrace.ImpactPoint,
																	 HitTrace.ImpactNormal.Rotation(),
																	 EAttachLocation::KeepWorldPosition,
																	 10.0f);
						}

						if (WeaponSetting.ProjectileSetting.HitFXs.Contains(HittedSurface->SurfaceType))
						{
							UParticleSystem* HittedParticle = WeaponSetting.ProjectileSetting.HitFXs[HittedSurface->SurfaceType];
							if (HittedParticle)
								UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
																		 HittedParticle,
																		 FTransform(HitTrace.ImpactNormal.Rotation(), HitTrace.ImpactPoint, FVector(1.0f)));
						}

						if (WeaponSetting.ProjectileSetting.HitSound)
						{
							UGameplayStatics::PlaySoundAtLocation(GetWorld(),
																  WeaponSetting.ProjectileSetting.HitSound,
																  HitTrace.ImpactPoint);
						}
						UGameplayStatics::ApplyDamage(HitTrace.GetActor(),
													  WeaponSetting.ProjectileSetting.ProjectileDamage,
													  GetInstigatorController(),
													  this,
													  NULL);
						// HittedComp->DestroyComponent();
					}
				}
			}				
	}
	OnWeaponFireStart.Broadcast();

	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		if (CheckCanWeaponReload())
			InitReload();
	}
}


FVector AWeaponDefault::GetFireEndLocation() const
{
	FVector EndLocation;
	FVector TmpVector = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	FVector DirectionShoot;

	if(TmpVector.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		DirectionShoot = ShootLocation->GetComponentLocation() - ShootEndLocation;
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((DirectionShoot).GetSafeNormal()) * -20000.0f;
		if(ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(),
						  -(DirectionShoot),
						  WeaponSetting.DistacneTrace,
						  GetCurrentDispersion()* PI / 180.f,
						  GetCurrentDispersion()* PI / 180.f,
						  32, FColor::Emerald,
						  false,
						  .1f,
						  (uint8)'\000',
						  1.0f);
	}	
	else
	{
		DirectionShoot = ShootLocation->GetForwardVector();
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(DirectionShoot) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(),
						  ShootLocation->GetComponentLocation(),
						  DirectionShoot,
						  WeaponSetting.DistacneTrace,
						  GetCurrentDispersion()* PI / 180.f,
						  GetCurrentDispersion()* PI / 180.f,
						  32, FColor::Emerald,
						  false,
						  .1f,
						  (uint8)'\000',
						  1.0f);
	}
		

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(),
					  ShootLocation->GetComponentLocation(),
					  ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f,
					  FColor::Cyan,
					  false,
					  5.f,
					  (uint8)'\000',
					  0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(),
					  ShootLocation->GetComponentLocation(),
					  ShootEndLocation,
					  FColor::Red,
					  false,
					  5.f,
					  (uint8)'\000',
					  0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(),
					  ShootLocation->GetComponentLocation(),
			          EndLocation,
			          FColor::Black,
			          false,
			          5.f,
			          (uint8)'\000',
			          0.5f);

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}
	
	return EndLocation;
}


/* get existing projectiles */
int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	int8 OutProjectile;
	if (AdditionalWeaponInfo.Round < WeaponSetting.NumberProjectileByShot)
		OutProjectile = AdditionalWeaponInfo.Round;
	else
		OutProjectile = WeaponSetting.NumberProjectileByShot;
	
	return OutProjectile;
}

// ============================================= Work With Fire Methods End ===================================== //
//																												  //
// ============================================= Work With Reloading Methods Begin ============================== //


void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}


void AWeaponDefault::InitReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponSetting.ReloadTime;

	//ToDo Anim reload
	
	
	OnWeaponReloadStart.Broadcast();
	
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	
	int8 AviableAmmoFromInventory = GetAviableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;

	if (NeedToReload > AviableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round = AviableAmmoFromInventory;
		AmmoNeedTakeFromInv = AviableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}
		
	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if(SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UInventoryComponent* MyInventory = Cast<UInventoryComponent>(GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
		if (MyInventory)
		{
			int8 AviableAmmoForWeapon;
			if (!MyInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				result = false;
			}
		}
	}

	return result;
}


int8 AWeaponDefault::GetAviableAmmoForReload()
{
	int8 AviableAmmoForWeapon = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UInventoryComponent* MyInv = Cast<UInventoryComponent>(GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
		if (MyInv)
		{			
			if (MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				//AviableAmmoForWeapon = AviableAmmoForWeapon;
			}
		}
	}
	return AviableAmmoForWeapon;
}

// ============================================= Work With Reloading Methods End ================================ //
//																												  //
// ============================================= Work With Dispersion Methods Begin ============================= //


void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}				

		if (CurrentDispersion < CurrentDispersionMin)
		{

			CurrentDispersion = CurrentDispersionMin;

		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if(ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}


void AWeaponDefault::UpdateStateWeapon(EMovementState* NewMovementState)
{
	//ToDo Dispersion
	BlockFire = false;

	switch (*NewMovementState)
	{
	case EMovementState::Aim_State:
		
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:
		
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::SprintRun_State:
		BlockFire = true;
		SetWeaponStateFire(false);//set fire trigger to false
		//Block Fire
		break;
	default:
		break;
	}
}


void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}


float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}


FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{		
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

// ============================================= Work With Dispersion Methods End ================================ //
//																												   //
// ============================================= Work With Debug Methods Begin =================================== //


// ============================================= Work With Debug Methods End ===================================== //
//																												   //
// ============================================= Work With Commons Methods Begin ================================= //

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}

	EMovementState Run_State = EMovementState::Run_State;
	
	UpdateStateWeapon(&Run_State);
}


int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}


void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;
			InitDropMesh(WeaponSetting.ShellBullets);
		}
		else
		{
			DropShellTimer -= DeltaTime;
		}
	}
}


void AWeaponDefault::InitDropMesh(FDropMeshInfo& DropMeshInfo)
{
	FTransform Transform = FTransform::Identity;
	FVector FinalDir = FVector::ZeroVector;
	
	if (DropMeshInfo.DropMesh)
	{
		
		FVector LocalDir = this->GetActorForwardVector() * DropMeshInfo.DropMeshOffset.GetLocation().X +
			this->GetActorRightVector() * DropMeshInfo.DropMeshOffset.GetLocation().Y +
			this->GetActorUpVector() * DropMeshInfo.DropMeshOffset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(DropMeshInfo.DropMeshOffset.GetScale3D());

		Transform.SetRotation((GetActorRotation() + DropMeshInfo.DropMeshOffset.Rotator()).Quaternion());

		FActorSpawnParameters Param;
		Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Param.Owner = this;
		AStaticMeshActor* NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

			NewActor->SetActorTickEnabled(false);
			NewActor->InitialLifeSpan = DropMeshInfo.DropMeshLifeTime;

			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMeshInfo.DropMesh);


			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);


			if (DropMeshInfo.CustomMass > 0.0f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, DropMeshInfo.CustomMass, true);
			}

			if (!DropMeshInfo.DropMeshImpulseDir.IsNearlyZero())
			{
				
				LocalDir += (DropMeshInfo.DropMeshImpulseDir * 1000.0f);

				if (!FMath::IsNearlyZero(DropMeshInfo.ImpulseRandomDispersion))
					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, DropMeshInfo.ImpulseRandomDispersion);

				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir.GetSafeNormal(0.0001f) * DropMeshInfo.PowerImpulse);
			}
		}
		
	}
}

void AWeaponDefault::InitClipDropMesh()
{
	InitDropMesh(WeaponSetting.ClipDropMesh);
}

// ============================================= Work With Commons Methods End =================================== //
//																												   //
// =============================================================================================================== //
