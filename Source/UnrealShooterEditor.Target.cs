// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class UnrealShooterEditorTarget : TargetRules
{
	public UnrealShooterEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("UnrealShooter");
		// bOverrideBuildEnvironment = true;
		// AdditionalCompilerArguments = "-Wno-unused-but-set-variable";

	}
}
